package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class NewQuery extends AppCompatActivity {
    Button a, b, c, d;  /* Şıklar */
    TextView question;  /* Soru */
    TextView timerText;  /* Timer 99.98.97*/
    Button[] pointButtons = new Button[12];  /* Sağ taraftaki Puan Butonları */

    TheSurvey survey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_query);
        getSupportActionBar().hide();
        LoadDesktopItems(); /* Ekrandaki Kontrolleri Al */
        try {
            survey = new TheSurvey(this, a, b, c, d, question);
            survey.setGainPoints(pointButtons);
            survey.setTimeText(timerText);

            survey.setSoruadet(12);
            survey.startSurvey();
        } catch (Exception e) {
            String x=e.getMessage();
        }
    }
    void LoadDesktopItems() {
        a = (Button) findViewById(R.id.a);
        b = (Button) findViewById(R.id.b);
        c = (Button) findViewById(R.id.c);
        d = (Button) findViewById(R.id.d);

        question = (TextView) findViewById(R.id.sorucontainer);
        pointButtons[0] = (Button) findViewById(R.id.score1);
        pointButtons[1] = (Button) findViewById(R.id.score2);
        pointButtons[2] = (Button) findViewById(R.id.score3);
        pointButtons[3] = (Button) findViewById(R.id.score4);
        pointButtons[4] = (Button) findViewById(R.id.score5);
        pointButtons[5] = (Button) findViewById(R.id.score6);
        pointButtons[6] = (Button) findViewById(R.id.score7);
        pointButtons[7] = (Button) findViewById(R.id.score7);
        pointButtons[8] = (Button) findViewById(R.id.score9);
        pointButtons[9] = (Button) findViewById(R.id.score10);
        pointButtons[10] = (Button) findViewById(R.id.score11);
        pointButtons[11] = (Button) findViewById(R.id.score12);
        timerText = (TextView) findViewById(R.id.timer);
    }
}
