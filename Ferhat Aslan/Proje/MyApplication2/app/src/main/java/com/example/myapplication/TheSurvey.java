package com.example.myapplication;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class TheSurvey {

    int questionWaitTimer = 2500;
    int newQuestionTime = 2500;

    static Activity activity;
    ArrayList<QuestionDb> theQuestions = new ArrayList<>();

    Button a;
    Button b;
    Button c;
    Button d;

    public static int elapsedTime = 0;              /* Geçen Süre */
    public static Timer timer = new Timer();        /* Sayaç */

    static String gainPoints;                       /* Kazanılan Puan : En Yüksek 143 olabilir */
    TextView questionText;                          /* Sorunun sorulacağı alan */
    QuestionDb question;                            /* Soru */
    TextView timeText;                              /* Sayaç Gösterim Text'i */

    Button[] pointBtn;                              /* Puan düğmesi */

    public static CountDownTimer countDownTimer;
    int index;
    int soruadet = 12;
    int currentQuestionIndex = 0;   /* Şu anda kaçıncı soru */


    static int cikisdurumu = 0;

    public TextView getTimeText() {
        return timeText;
    }

    public void setTimeText(TextView timeText) {
        this.timeText = timeText;
    }

    public Button[] getPointBtn() {
        return pointBtn;
    }

    public void setGainPoints(Button[] pointBtn) {
        this.pointBtn = pointBtn;
    }

    public int getSoruadet() {
        return soruadet;
    }

    public void setSoruadet(int soruadet) {
        this.soruadet = soruadet;
    }


    public TheSurvey(final Activity activity, Button a, Button b, Button c, Button d, TextView questionText) {
        /* Constructor */
        this.activity = activity;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.questionText = questionText;
        theQuestions=getQuestions();            /* XML Veritabanından Soruları Getir */
    }

    public ArrayList<QuestionDb> getQuestions() {
        ArrayList<QuestionDb> Q = new ArrayList<>();
        XmlPullParserFactory pullParserFactory;
        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            InputStream in_s = activity.getApplicationContext().getAssets().open("question.xml"); /* Soruların okunacağı dosya adı */
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);
            Q = parseXML(parser);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.shuffle(Q); //soruları karma
        return Q;
    }
    public interface answer {
        void answerIsCorrect();
        void answerIsWrong();
    }

    private ArrayList<QuestionDb> parseXML(XmlPullParser parser) throws XmlPullParserException, IOException {
        String text = "";
        ArrayList<QuestionDb> sorular = new ArrayList<>();
        int eventType = parser.getEventType();
        QuestionDb soru = null;
        Map<String, String> cevaplar = new HashMap<>();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagname = parser.getName();
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagname.equalsIgnoreCase("Questions")) {
                        soru = new QuestionDb();
                    }
                    break;

                case XmlPullParser.TEXT:
                    text = parser.getText();
                    break;

                case XmlPullParser.END_TAG:
                    if (tagname.equalsIgnoreCase("Questions")) {
                        soru.setAnswers(cevaplar);
                        cevaplar = new HashMap<>();
                        sorular.add(soru);
                    } else if (tagname.equalsIgnoreCase("Question")) {
                        soru.setTheQuestion(text);
                    } else if (tagname.equalsIgnoreCase("a")) {
                        cevaplar.put("1", text);
                    } else if (tagname.equalsIgnoreCase("b")) {
                        cevaplar.put("2", text);
                    } else if (tagname.equalsIgnoreCase("c")) {
                        cevaplar.put("3", text);
                    } else if (tagname.equalsIgnoreCase("d")) {
                        cevaplar.put("4", text);
                    } else if (tagname.equalsIgnoreCase("Answer")) {
                        soru.setCorrectAnswer(text);
                    }
                    break;

                default:
                    break;
            }
            eventType = parser.next();
        }
        text="Test";
        return sorular;
    }

    public void startSurvey() {
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                elapsedTime++;
                //Log.e("Sure:::", "" + elapsedTime);
            }
        };
        currentQuestionIndex=1;
        timer = new Timer();
        timer.scheduleAtFixedRate(t, 1000, 1000);
        askQuestion(currentQuestionIndex, new answer() {
            @Override
            public void answerIsCorrect() {
            }
            @Override
            public void answerIsWrong() {
                closeSurvey();
            }
        });
    }

    public String setKazanilanPara(int QQ) {
        String kazanilantutarsoru = "0";

        for (int i = 0; i < getPointBtn().length; i++) {

            if (i == QQ) {
                getPointBtn()[i].setBackground(activity.getDrawable(R.drawable.para_aktifsira));
            } else if ((i == 1 || i == 6 || i == 10) && i != QQ) {
                getPointBtn()[i].setBackground(activity.getDrawable(R.drawable.para_barajsira));
            } else {
                getPointBtn()[i].setBackground(activity.getDrawable(R.drawable.para_normalsira));
            }
        }
        switch (QQ) {
            case 0:
                kazanilantutarsoru = "1";
                break;
            case 1:
                kazanilantutarsoru = "1";
                break;
            case 2:
                kazanilantutarsoru = "2";
                break;
            case 3:
                kazanilantutarsoru = "3";
                break;
            case 4:
                kazanilantutarsoru = "5";
                break;
            case 5:
                kazanilantutarsoru = "8";
                break;
            case 6:

                kazanilantutarsoru = "13";
                break;
            case 7:
                kazanilantutarsoru = "21";
                break;
            case 8:
                kazanilantutarsoru = "34";
                break;
            case 9:

                kazanilantutarsoru = "55";
                break;
            case 10:
                kazanilantutarsoru = "89";
                break;
            case 11:
                kazanilantutarsoru = "143";
                break;        }

        return kazanilantutarsoru;
    }
    public CountDownTimer downTimer() {
        countDownTimer = new CountDownTimer(24000, 1000) {

            public void onTick(long millisUntilFinished) {
                getTimeText().setText("" + millisUntilFinished / 1000);
                if (getTimeText().getText().equals("1")) {
                    closeSurvey(); /* Yarışmayı sonlandır */
                }
            }
            public void onFinish() {
            }
        };
        return countDownTimer;
    }
    void askQuestion(int questionIndex, final answer cevap) {
        index = questionIndex;
        boolean sonuc = false;
        question =theQuestions.get(questionIndex);
        Map<String, String> cevaplar = new HashMap<>();
        cevaplar = question.getAnswers();
        a.setVisibility(View.VISIBLE);
        b.setVisibility(View.VISIBLE);
        c.setVisibility(View.VISIBLE);
        d.setVisibility(View.VISIBLE);
        a.setText("A:  " + cevaplar.get("1"));
        b.setText("B:  " + cevaplar.get("2"));
        c.setText("C:  " + cevaplar.get("3"));
        d.setText("D:  " + cevaplar.get("4"));
        a.setEnabled(true);
        b.setEnabled(true);
        c.setEnabled(true);
        d.setEnabled(true);
        final CountDownTimer ct =downTimer();
        ct.start(); /* Sayacı Çalıştır */
        gainPoints = setKazanilanPara(index);
        a.setBackground(activity.getDrawable(R.drawable.sorunormalcevap));
        a.setTextColor(Color.WHITE);
        b.setBackground(activity.getDrawable(R.drawable.sorunormalcevap));
        b.setTextColor(Color.WHITE);
        c.setBackground(activity.getDrawable(R.drawable.sorunormalcevap));
        c.setTextColor(Color.WHITE);
        d.setBackground(activity.getDrawable(R.drawable.sorunormalcevap));
        d.setTextColor(Color.WHITE);
        questionText.setText(question.getTheQuestion());
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // SesCalIkiSaniye(R.raw.sikisaretleme);
                DogruButtonHerzamanYesil(question.getCorrectAnswer());
                ct.cancel();
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                a.setBackground(activity.getDrawable(R.drawable.soruaktifsari));
                a.setTextColor(Color.BLACK);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (a.getTag().equals(question.getCorrectAnswer())) {
                           //SesCalIkiSaniye(R.raw.dogrucevap);
                            a.setBackground(activity.getDrawable(R.drawable.sorudogrucevap));
                            a.setTextColor(Color.WHITE);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsCorrect();
                                    if (index < (getSoruadet() - 1)) {
                                        index++;
                                        askQuestion(index, new answer() {
                                            @Override
                                            public void answerIsCorrect() {
                                                //askQuestion(index,cevap);
                                            }

                                            @Override
                                            public void answerIsWrong() {
                                                closeSurvey();
                                                Log.e("Çıkış Komutu", "A şıkkı yanlış cevapdan geldi");
                                            }
                                        });
                                    } else {
                                        closeSurveyFinal();
                                    }
                                }
                            }, newQuestionTime);
                        } else {
                           // SesCal(R.raw.yanliscevap);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsWrong();

                                }
                            }, newQuestionTime);
                            a.setBackground(activity.getDrawable(R.drawable.yanliscevap));
                            a.setTextColor(Color.WHITE);
                        }
                    }
                }, questionWaitTimer);
            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DogruButtonHerzamanYesil(question.getCorrectAnswer());
                ct.cancel();
                b.setEnabled(false);
                a.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                b.setBackground(activity.getDrawable(R.drawable.soruaktifsari));
                b.setTextColor(Color.BLACK);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (b.getTag().equals(question.getCorrectAnswer())) {

                            b.setBackground(activity.getDrawable(R.drawable.sorudogrucevap));
                            b.setTextColor(Color.WHITE);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsCorrect();
                                    if (index < (getSoruadet() - 1)) {
                                        index++;
                                        askQuestion(index, new answer() {
                                            @Override
                                            public void answerIsCorrect() {
                                               // askQuestion(index,cevap);
                                            }

                                            @Override
                                            public void answerIsWrong() {
                                                closeSurvey();
                                                Log.e("Çıkış Komutu", "B şıkkı yanlış cevapdan geldi");
                                            }
                                        });
                                    } else {
                                        closeSurveyFinal();
                                    }
                                }
                            }, newQuestionTime);
                        } else {
                           //SesCal(R.raw.yanliscevap);

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsWrong();
                                }
                            }, newQuestionTime);
                            b.setBackground(activity.getDrawable(R.drawable.yanliscevap));
                            b.setTextColor(Color.WHITE);
                        }
                    }
                }, questionWaitTimer);
            }
        });
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DogruButtonHerzamanYesil(question.getCorrectAnswer());
                ct.cancel();
                c.setEnabled(false);
                b.setEnabled(false);
                a.setEnabled(false);
                d.setEnabled(false);
                c.setBackground(activity.getDrawable(R.drawable.soruaktifsari));
                c.setTextColor(Color.BLACK);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (c.getTag().equals(question.getCorrectAnswer())) {
                            c.setBackground(activity.getDrawable(R.drawable.sorudogrucevap));
                            c.setTextColor(Color.WHITE);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsCorrect();
                                    if (index < (getSoruadet() - 1)) {
                                        index++;
                                        askQuestion(index, new answer() {
                                            @Override
                                            public void answerIsCorrect() {
                                               // askQuestion(index,cevap);
                                            }

                                            @Override
                                            public void answerIsWrong() {
                                                closeSurvey();
                                                Log.e("Çıkış Komutu", "C şıkkı yanlış cevapdan geldi");
                                            }
                                        });
                                    } else {
                                        closeSurveyFinal();
                                    }
                                }
                            }, newQuestionTime);
                        } else {
                            //SesCal(R.raw.yanliscevap);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsWrong();
                                }
                            }, newQuestionTime);
                            c.setBackground(activity.getDrawable(R.drawable.yanliscevap));
                            c.setTextColor(Color.WHITE);
                        }
                    }
                }, questionWaitTimer);
            }
        });
        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DogruButtonHerzamanYesil(question.getCorrectAnswer());
                ct.cancel();
                d.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                a.setEnabled(false);
                d.setBackground(activity.getDrawable(R.drawable.soruaktifsari));
                d.setTextColor(Color.BLACK);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (d.getTag().equals(question.getCorrectAnswer())) {
                            d.setBackground(activity.getDrawable(R.drawable.sorudogrucevap));
                            d.setTextColor(Color.WHITE);
                            final Handler handler = new Handler();
                            cevap.answerIsCorrect();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (index < (getSoruadet() - 1)) {
                                        index++;
                                        askQuestion(index, new answer() {
                                            @Override
                                            public void answerIsCorrect() {
                                                //askQuestion(index,cevap);
                                            }

                                            @Override
                                            public void answerIsWrong() {
                                                closeSurvey();
                                            }
                                        });
                                    } else {
                                        closeSurveyFinal();
                                    }
                                }
                            }, newQuestionTime);
                        } else {


                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    cevap.answerIsWrong();
                                }
                            }, newQuestionTime);
                            d.setBackground(activity.getDrawable(R.drawable.yanliscevap));
                            d.setTextColor(Color.WHITE);
                        }
                    }
                }, questionWaitTimer);
            }
        });
    }

    public static void closeSurvey() {
        FragmentManager fm = activity.getFragmentManager();
        EndOfSurveyAll dialogFragment = new EndOfSurveyAll();
    }


    public static void closeSurveyFinal() {
        FragmentManager fm = activity.getFragmentManager();
        EndOfSurveyAll dialogFragment = new EndOfSurveyAll();

    }
    public void DogruButtonHerzamanYesil(final String dogruccevap)
    {
        final Button dogrucevapbuttonu;
        if (a.getTag().equals(dogruccevap)) {
            dogrucevapbuttonu = a;
        } else if (b.getTag().equals(dogruccevap)) {
            dogrucevapbuttonu = b;
        } else if (c.getTag().equals(dogruccevap)) {
            dogrucevapbuttonu = c;
        } else {
            dogrucevapbuttonu = d;
        }
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dogrucevapbuttonu.setBackground(activity.getDrawable(R.drawable.sorudogrucevap));
            }
        },questionWaitTimer);
}

    public static class EndOfSurveyAll extends DialogFragment
    {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.survey_question, container, false);
            getDialog().setTitle("Kazandığınız puan");
            TextView para = (TextView) rootView.findViewById(R.id.kazandiginpara);
            String paraver = "1";
            countDownTimer.cancel();
            para.setText(paraver);
            timer.cancel();
            Log.e("Bitiş süresi", "" + elapsedTime);
            Winner basari = new Winner(paraver, elapsedTime);
            Log.e("basari puani:", "" + basari.getPuan());
            TextView aciklama = (TextView) rootView.findViewById(R.id.aciklama);
            aciklama.setText(getActivity().getResources().getString(R.string.game_over_final));
            this.setCancelable(false);
            Button anamenu = (Button) rootView.findViewById(R.id.anasayfabutton);
            anamenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finish();
                }
            });
            return rootView;
        }
    }
}
