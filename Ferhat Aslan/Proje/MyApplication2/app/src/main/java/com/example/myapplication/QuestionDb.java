package com.example.myapplication;

import java.util.Map;

/* QuestionDB Sınıfı : XML Veritabanındaki soru, şık ve doğru yanıtın tanımlandığı sınıftır */
public class QuestionDb {
    Map<String,String> answers;
    String correctAnswer;
    String theQuestion;

public QuestionDb() {
    }

    public Map<String, String> getAnswers() {

        return answers;
    }

    public void setAnswers(Map<String, String> cevaplar) {
        this.answers = cevaplar;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String dogrucevap) {
        this.correctAnswer = dogrucevap;
    }

    public String getTheQuestion() {
        return theQuestion;
    }

    public void setTheQuestion(String sorulansoru) {
        this.theQuestion = sorulansoru;
    }

    public QuestionDb(Map<String, String> answer , String correctAnswer, String theQuestion) {

        this.answers = answer;
        this.correctAnswer = correctAnswer;
        this.theQuestion = theQuestion;
    }
}
